import scrapy


class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    start_urls = ["http://quotes.toscrape.com/tag/humor/"]

    def parse(self, response):
        all_div = response.css("div.quote")
        for quotes in all_div:
            quote = quotes.css('span.text::text').extract()
            name = quotes.css("small.author::text").extract()
            meta = quotes.css(".tag::text").extract()
            yield {
                'quote': quote,
                 'author_name': name,
                'content_of_book': meta
             }
